# Translation of docs_krita_org_general_concepts___colors___viewing_conditions.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___colors___viewing_conditions\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 14:51+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_example_metamerism.png"
msgstr ".. image:: images/color_category/Krita_example_metamerism.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mixup_ex1_02.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_02.png"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"
msgstr ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_metamerism_presentation.svg"
msgstr ".. image:: images/color_category/Krita_metamerism_presentation.svg"

#: ../../general_concepts/colors/viewing_conditions.rst:1
msgid "What are viewing conditions."
msgstr "Пояснення щодо умов перегляду."

#: ../../general_concepts/colors/viewing_conditions.rst:10
#: ../../general_concepts/colors/viewing_conditions.rst:15
msgid "Viewing Conditions"
msgstr "Умови перегляду"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Metamerism"
msgstr "Метамерія"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Color"
msgstr "Колір"

#: ../../general_concepts/colors/viewing_conditions.rst:17
msgid ""
"We mentioned viewing conditions before, but what does this have to do with "
"'white points'?"
msgstr ""
"Ми вже згадували умови перегляду, але що спільного у цього поняття із "
"«точками білого кольору»?"

#: ../../general_concepts/colors/viewing_conditions.rst:19
msgid ""
"A lot actually, rather, white points describe a type of viewing condition."
msgstr ""
"Насправді, дуже багато. Точки білого кольору описують тип умов спостереження."

#: ../../general_concepts/colors/viewing_conditions.rst:21
msgid ""
"So, usually what we mean by viewing conditions is the lighting and "
"decoration of the room that you are viewing the image in. Our eyes try to "
"make sense of both the colors that you are looking at actively (the colors "
"of the image) and the colors you aren't looking at actively (the colors of "
"the room), which means that both sets of colors affect how the image looks."
msgstr ""
"Отже, зазвичай, під умовами перегляду ми маємо на увазі освітлення та декор "
"приміщення, у якому ми переглядаємо зображення. Наші очі намагаються взяти "
"до уваги одразу кольори, на які ми дивимося активно, (кольори зображення) та "
"кольори, на які ми не дивимося активно (кольори приміщення), а це означає, "
"що на те, як виглядає зображення, впливають одразу обидва набори кольорів."

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"
msgstr ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ""
"**Left**: Let's ruin Vermeer by putting a bright purple background that asks "
"for more attention than the famous painting itself. **Center**: a much more "
"neutral backdrop that an interior decorator would hate but brings out the "
"colors. **Right**: The approximate color that this painting is displayed "
"against in real life in the Maurits House, at the least, last time I was "
"there. Original image from wikipedia commons."
msgstr ""
"**Ліворуч**: зіпсуємо Вермера, додавши яскраво-пурпурове тло, яке "
"привертатиме більше уваги за саму відому картину. **У центрі**: набагато "
"нейтральніше тло, яке не сподобається оформлювачу інтер'єра, але підкреслить "
"кольори. **Праворуч**: приблизний колір, на тлі якого цю картину виставлено "
"у Королівській галереї Мауріцгейс, принаймні на момент відвідування її "
"автором підручника. Початкове зображення взято зі сховища зображень "
"Вікіпедії."

#: ../../general_concepts/colors/viewing_conditions.rst:29
msgid ""
"This is for example, the reason why museum exhibitioners can get really "
"angry at the interior decorators when the walls of the museum are painted "
"bright red or blue, because this will drastically change the way how the "
"painting's colors look. (Which, if we are talking about a painter known for "
"their colors like Vermeer, could result in a really bad experience)."
msgstr ""
"За цим прикладом можна зрозуміти причину, через яку кураторів музейних "
"виставок може дуже роздратувати бажання декораторів інтер'єра пофарбувати "
"стіни у яскравий червоний або синій колір. Такі фарби стін можуть разюче "
"змінити вигляд картин. А це, якщо ми розглядатимемо картини художників, які "
"відомі тонким відчуттям кольорів, зокрема Вермера, може призвести до "
"жахливих наслідків."

#: ../../general_concepts/colors/viewing_conditions.rst:37
msgid ""
"Lighting is the other component of the viewing condition which can have "
"dramatic effects. Lighting in particular affects the way how all colors "
"look. For example, if you were to paint an image of sunflowers and poppies, "
"print that out, and shine a bright yellow light on it, the sunflowers would "
"become indistinguishable from the white background, and the poppies would "
"look orange. This is called `metamerism <https://en.wikipedia.org/wiki/"
"Metamerism_%28color%29>`_, and it's generally something you want to avoid in "
"your color management pipeline."
msgstr ""
"Освітлення є іншим компонентом умов перегляду. Цей компонент може мати "
"величезний вплив на вигляд зображення. Зокрема, освітлення впливає на вигляд "
"усіх кольорів. Наприклад, якщо ви намалюєте картину з соняшниками і маками, "
"надрукуєте її і скористаєтеся яскравим жовтуватим освітленням, соняшники "
"буде не відрізнити від білого тла, а маки виглядатимуть помаранчевими. Цей "
"ефект називається `метамерією <https://uk.wikipedia.org/wiki/%D0%9C"
"%D0%B5%D1%82%D0%B0%D0%BC%D0%B5%D1%80%D1%96%D1%8F_(%D0%BA%D0%BE%D0%BB"
"%D1%96%D1%80)>`_. Загалом, це те, чого слід уникати у конвеєрі керування "
"кольорами."

#: ../../general_concepts/colors/viewing_conditions.rst:39
msgid ""
"An example where metamerism could become a problem is when you start "
"matching colors from different sources together."
msgstr ""
"Прикладом проблем, які пов'язано із метамерією, є випадок порівняння "
"кольорів з різних джерел."

#: ../../general_concepts/colors/viewing_conditions.rst:46
msgid ""
"For example, if you are designing a print for a red t-shirt that's not "
"bright red, but not super grayish red either. And you want to make sure the "
"colors of the print match the color of the t-shirt, so you make a dummy "
"background layer that is approximately that red, as correctly as you can "
"observe it, and paint on layers above that dummy layer. When you are done, "
"you hide this dummy layer and sent the image with a transparent background "
"to the press."
msgstr ""
"Наприклад, нехай ви створюєте дизайн для друку червоної футболки, колір якої "
"не є яскравим, але і не є зовсім тьмяним червоним. Ви хочете, щоб кольори "
"надрукованого зображення добре поєднувалися із кольором футболки. Отже, ви "
"створюєте фіктивний шар тла, який буде приблизно таким самим червоним, так, "
"як ви бачите цей колір, і малюєте на шарах над цим фіктивним шаром. Коли усе "
"готове, ви приховуєте цей фіктивний шар і надсилаєте зображення із прозорим "
"тлом для друку."

#: ../../general_concepts/colors/viewing_conditions.rst:54
msgid ""
"But when you get the t-shirt from the printer, you notice that all your "
"colors look off, mismatched, and maybe too yellowish (and when did that T-"
"Shirt become purple?)."
msgstr ""
"Але коли ви отримаєте футболку після друку, ви зауважите, що усі ваші "
"кольори виглядають не так, якось інакше і, мабуть, жовтувато (і коли це "
"футболка стала пурпуровою?)."

#: ../../general_concepts/colors/viewing_conditions.rst:56
msgid "This is where white points come in."
msgstr "Ось тут проблема виникла через точку білого кольору."

#: ../../general_concepts/colors/viewing_conditions.rst:58
msgid ""
"You probably observed the t-shirt in a white room where there were "
"incandescent lamps shining, because as a true artist, you started your work "
"in the middle of the night, as that is when the best art is made. However, "
"incandescent lamps have a black body temperature of roughly 2300-2800K, "
"which makes them give a yellowish light, officially called White Point A."
msgstr ""
"Ймовірно, ви бачили футболку у кімнаті з білими стінами, у світлі ламп "
"розжарювання, оскільки як справжній художник розпочали роботу вночі, коли "
"найлегше творити. Втім, випромінювання ламп розжарювання відповідає "
"температурі абсолютно чорного тіла приблизно 2300-2800K, тобто ці лампи "
"дають жовтувате світло, яке офіційно називають «Точка білого A»."

#: ../../general_concepts/colors/viewing_conditions.rst:61
msgid ""
"Your computer screen on the other hand, has a black body temperature of "
"6500K, also known as D65. Which is a far more blueish color of light than "
"the lamps you are hanging."
msgstr ""
"З іншого боку, екран вашого комп'ютера має відповідну температуру абсолютно "
"чорного тіла 6500K, також відома як D65. Це набагато синіший колір світла, "
"ніж у ламп."

#: ../../general_concepts/colors/viewing_conditions.rst:63
msgid ""
"What's worse, Printers print on the basis of using a white point of D50, the "
"color of white paper under direct sunlight."
msgstr ""
"Погіршує ситуацію те, що принтери друкують з використанням точки білого D50, "
"кольору білого паперу, який освітлено прямим сонячним промінням."

#: ../../general_concepts/colors/viewing_conditions.rst:70
msgid ""
"So, by eye-balling your t-shirt's color during the evening, you took its red "
"color as transformed by the yellowish light. Had you made your observation "
"in diffuse sunlight of an overcast (which is also roughly D65), or made it "
"in direct sunlight light and painted your picture with a profile set to D50, "
"the color would have been much closer, and thus your design would not be as "
"yellowish."
msgstr ""
"Тобто, розглядаючи кольори вашої футболки вночі, ви сприйняли її червоний "
"колір у жовтуватому освітленні. Якби ви спостерігали його у розсіяному "
"денному світлі у похмурий день (приблизно D65) або створили зображення в "
"умовах безпосереднього сонячного освітлення і намалювати вашу картинку з "
"використанням профілю D50, колір би був набагато ближчим, отже створене вами "
"зображення не було б таким жовтуватим."

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ".. image:: images/color_category/White_point_mixup_ex1_03.png"
msgstr ".. image:: images/color_category/White_point_mixup_ex1_03.png"

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ""
"Applying a white balance filter will sort of match the colors to the tone as "
"in the middle, but you would have had a much better design had you designed "
"against the actual color to begin with."
msgstr ""
"Застосування фільтра балансу білого певним чином встановити відповідність "
"кольорів за середнім тоном, але набагато кращих результатів можна досягти, "
"якщо від початку створювати зображення для отримання бажаного кольору "
"результату."

#: ../../general_concepts/colors/viewing_conditions.rst:79
msgid ""
"Now, you could technically quickly fix this by using a white balancing "
"filter, like the ones in G'MIC, but because this error is caught at the end "
"of the production process, you basically limited your use of possible colors "
"when you were designing, which is a pity."
msgstr ""
"Тепер, з технічної точки зору, ви могли б швидко виправити це застосуванням "
"фільтра балансування білого, зокрема фільтра з G'MIC, але оскільки цю "
"помилку виявлено наприкінці процедури виробництва, вас, на жаль, буде "
"обмежено у використанні можливих кольорів при створення продукції."

#: ../../general_concepts/colors/viewing_conditions.rst:81
msgid ""
"Another example where metamerism messes things up is with screen projections."
msgstr ""
"Іншим прикладом, коли метамерія псує зображення, є проєктування зображення "
"на екран."

#: ../../general_concepts/colors/viewing_conditions.rst:83
msgid ""
"We have a presentation where we mark one type of item with red, another with "
"yellow and yet another with purple. On a computer the differences between "
"the colors are very obvious."
msgstr ""
"У нас була презентація, де ми намалювали одну частину кругу червоним, іншу — "
"жовтим, а решту — пурпуровим. На комп'ютерному екрані відмінність між усіма "
"цими кольорами є очевидною."

#: ../../general_concepts/colors/viewing_conditions.rst:89
msgid ""
"However, when we start projecting, the lights of the room aren't dimmed, "
"which means that the tone scale of the colors becomes crunched, and yellow "
"becomes near indistinguishable from white. Furthermore, because the light in "
"the room is slightly yellowish, the purple is transformed into red, making "
"it indistinguishable from the red. Meaning that the graphic is difficult to "
"read."
msgstr ""
"Втім, під час проєктування на екран світло у кімнаті буде увімкнено, а тони "
"кольорів спотворено, і жовтий стало важко відрізнити від білого. Крім того, "
"оскільки світло є дещо жовтуватим, пурпуровий набув червонуватого відтінку, "
"його було важко відрізнити від червоного. Тому із нашою графікою було важко "
"розібратися."

#: ../../general_concepts/colors/viewing_conditions.rst:91
msgid ""
"In both cases, you can use Krita's color management a little to help you, "
"but mostly, you just need to be ''aware'' of it, as Krita can hardly fix "
"that you are looking at colors at night, or the fact that the presentation "
"hall owner refuses to turn off the lights."
msgstr ""
"У обох випадках ви можете скористатися засобами керування кольорами Krita "
"для виправлення ситуації, але, найкраще, просто мати це на увазі, оскільки "
"Krita навряд чи зможе виправити те, що ви розглядатимете кольори вночі, чи "
"те, що власник приміщення для презентації не хоче вимикати світло."

#: ../../general_concepts/colors/viewing_conditions.rst:93
msgid ""
"That said, unless you have a display profile that uses LUTs, such as an OCIO "
"LUT or a cLUT icc profile, white point won't matter much when choosing a "
"working space, due to weirdness in the icc v4 workflow which always converts "
"matrix profiles with relative colorimetric, meaning the white points are "
"matched up."
msgstr ""
"Інакше кажучи, якщо у вас немає профілю дисплея, який використовує таблиці "
"пошуку (LUT), зокрема OCIO або профіль ICC cLUT, точка білого не багато чого "
"змінить при виборі робочого простору через особливості у робочому процесі із "
"ICC v4, при якому профілі матриці завжди перетворюються на відносні "
"колориметричні, що означає, що встановлюється відповідність між точками "
"білого."
