# Translation of docs_krita_org_reference_manual___layers_and_masks___clone_layers.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___clone_layers\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:46+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:1
msgid "How to use clone layers."
msgstr "Як користуватися шарами клонування."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Linked Clone"
msgstr "Пов'язаний клон"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Clone Layer"
msgstr "Шар клонування"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:20
msgid "Clone Layers"
msgstr "Шари клонування"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:22
msgid ""
"A clone layer is a layer that keeps an up-to-date copy of another layer. You "
"cannot draw or paint on it directly, but it can be used to create effects by "
"applying different types of layers and masks (e.g. filter layers or masks)."
msgstr ""
"Шар клонування — шар, у якому зберігається синхронізована копія іншого шару. "
"Ви не можете креслити або малювати щось на цьому шарі безпосередньо, але ним "
"можна скористатися для створення ефектів, застосовуючи різні типи шарів та "
"масок (наприклад шари або маски фільтрування)."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:25
msgid "Example uses of Clone Layers."
msgstr "Приклад використання шарів клонування."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:27
msgid ""
"For example, if you were painting a picture of some magic person and wanted "
"to create a glow around them that was updated as you updated your character, "
"you could:"
msgstr ""
"Наприклад, якщо ви малюєте якусь магічну істоту і хочете створити сяйво "
"навколо неї, яке автоматично оновлюватиметься відповідно до змін, які ви "
"вноситимете до малюнка вашого персонажа, ви можете виконати такі дії:"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:29
msgid "Have a Paint Layer where you draw your character"
msgstr "Створити шар малювання, де ви малюватимете вашого персонажа."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:30
msgid ""
"Use the Clone Layer feature to create a clone of the layer that you drew "
"your character on"
msgstr ""
"Скористатися можливістю клонування шарів для створення клону шару, на якому "
"намальовано персонаж."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:31
msgid ""
"Apply an HSV filter mask to the clone layer to make the shapes on it white "
"(or blue, or green etc.)"
msgstr ""
"Застосувати маску фільтрування HSV до клонованого шару для того, щоб форми "
"на ньому стали білими (або синіми чи зеленими тощо)."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:32
msgid "Apply a blur filter mask to the clone layer so it looks like a \"glow\""
msgstr ""
"Застосувати маску розмивання до шару клонування, щоб створити для форм ефект "
"«сяйва»."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:34
msgid ""
"As you keep painting and adding details, erasing on the first layer, Krita "
"will automatically update the clone layer, making your \"glow\" apply to "
"every change you make."
msgstr ""
"Коли ви малюватимете і додаватимете деталі або витиратимете щось на першому "
"шарі, Krita автоматично оновлюватиме зображення на клонованому шарі, "
"застосовуючи ефект «сяйва» до усіх внесених вами змін."
