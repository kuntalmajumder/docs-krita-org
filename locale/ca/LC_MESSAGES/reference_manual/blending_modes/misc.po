# Translation of docs_krita_org_reference_manual___blending_modes___misc.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 15:53+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""
"Pàgina sobre els diversos modes de barreja al Krita: Mapa de relleu, Combina "
"mapa normal, Copia el vermell, Copia el verd, Copia el blau, Copia i Dissol."

#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr "Miscel·lània"

#: ../../reference_manual/blending_modes/misc.rst:17
msgid "Bumpmap (Blending Mode)"
msgstr "Mapa de relleu (mode de barreja)"

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr "Mapa de relleu"

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr "Aquest filtre sembla multiplicar i respectar l'alfa de l'entrada."

#: ../../reference_manual/blending_modes/misc.rst:25
#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr "Combina mapa normal"

#: ../../reference_manual/blending_modes/misc.rst:25
msgid "Normal Map"
msgstr "Mapa normal"

# skip-rule: t-acc_obe
#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <https://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""
"Mode de barreja matemàticament robust per a mapes normals, utilitzant la "
"`Barreja de mapa normal reorientada <https://blog.selfshadow.com/"
"publications/blending-in-detail/>`_."

#: ../../reference_manual/blending_modes/misc.rst:34
msgid "Copy (Blending Mode)"
msgstr "Copia (mode de barreja)"

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr "Copia"

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr ""
"Copia exactament la capa anterior. Útil per a quan s'utilitzen filtres i "
"màscares de filtratge."

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr "Esquerra: **Normal**. Dreta: **Copia**."

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Red"
msgstr "Copia el vermell"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Green"
msgstr "Copia el verd"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Blue"
msgstr "Copia el blau"

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr "Copia el vermell, verd, blau"

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr ""
"Aquesta és un mode de barreja que només copiarà/barrejarà un canal d'origen "
"a un canal de destinació. Específicament, prendrà el canal específic de la "
"capa superior i el copiarà a les capes inferiors."

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""
"Per tant, si voleu que el pinzell només afecti el canal vermell, establiu el "
"mode de barreja a «Copia el vermell»."

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"
msgstr ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""
"Els modes de barreja copia el vermell, verd i blau també funcionen sobre les "
"capes de filtratge."

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""
"Això també es pot fer amb les capes de filtratge. Per tant, si voleu canviar "
"ràpidament el canal verd d'una capa, creeu una capa de filtratge invers amb "
"un «Copia el verd» a sobre."

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr "Esquerra: **Normal**. Dreta: **Copia el vermell**."

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr "Esquerra: **Normal**. Dreta: **Copia el verd**."

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr "Esquerra: **Normal**. Dreta: **Copia el blau**."

#: ../../reference_manual/blending_modes/misc.rst:86
#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr "Dissol"

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr ""
"En lloc d'utilitzar la transparència, aquest mode de barreja utilitzarà un "
"patró de juxtaposició a l'atzar per a fer que les àrees transparents semblin "
"una mica transparents."

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr "Esquerra: **Normal**. Dreta: **Dissol**."
