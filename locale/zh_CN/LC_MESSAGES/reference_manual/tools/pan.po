msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___pan.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"

#: ../../<rst_epilog>:80
msgid ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: toolpan"
msgstr ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: toolpan"

#: ../../reference_manual/tools/pan.rst:1
msgid "Krita's pan tool reference."
msgstr ""

#: ../../reference_manual/tools/pan.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/pan.rst:11
msgid "Pan"
msgstr ""

#: ../../reference_manual/tools/pan.rst:16
msgid "Pan Tool"
msgstr "平移工具"

#: ../../reference_manual/tools/pan.rst:18
msgid "|toolpan|"
msgstr ""

#: ../../reference_manual/tools/pan.rst:20
msgid ""
"The pan tool allows you to pan your canvas around freely. It can be found at "
"the bottom of the toolbox, and you just it by selecting the tool, and doing |"
"mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:22
msgid ""
"There are two hotkeys associated with this tool, which makes it easier to "
"access from the other tools:"
msgstr ""

#: ../../reference_manual/tools/pan.rst:24
msgid ":kbd:`Space +` |mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:25
msgid "|mousemiddle| :kbd:`+ drag` over the canvas."
msgstr ""

#: ../../reference_manual/tools/pan.rst:27
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
