# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:50+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../tutorials/saving-for-the-web.rst:1
msgid "Tutorial for saving images for the web"
msgstr "Handledning för att spara bilder för webben"

#: ../../tutorials/saving-for-the-web.rst:13
msgid "Saving For The Web"
msgstr "Spara för webben"

#: ../../tutorials/saving-for-the-web.rst:15
msgid ""
"Krita's default saving format is the :ref:`file_kra` format. This format "
"saves everything Krita can manipulate about an image: Layers, Filters, "
"Assistants, Masks, Color spaces, etc. However, that's a lot of data, so ``*."
"kra`` files are pretty big. This doesn't make them very good for uploading "
"to the internet. Imagine how many people's data-plans hit the limit if they "
"only could look at ``*.kra`` files! So instead, we optimise our images for "
"the web."
msgstr ""
"Kritas standardformat för att spara är formatet :ref:`file_kra`. Formatet "
"sparar allting Krita kan manipulera i en bild: lager, filter, guide, masker, "
"färgrymder, etc. Dock är det en stor mängd data, så  ``*.kra`` filer är rätt "
"stora. Det gör dem inte särskilt bra att ladda upp till Internet. Föreställ "
"dig hur många personer som skulle nå sina surfgränser om de bara kunde titta "
"på ``*.kra`` filer. Istället optimerar vi våra bilder för webben."

#: ../../tutorials/saving-for-the-web.rst:17
msgid "There are a few steps involved:"
msgstr "Det omfattar några steg:"

#: ../../tutorials/saving-for-the-web.rst:19
msgid ""
"Save as a ``.kra``. This is your working file and serves as a backup if you "
"make any mistakes."
msgstr ""
"Spara som ``.kra``. Det är arbetsfilen och fungerar som säkerhetskopia om "
"man göra några misstag."

#: ../../tutorials/saving-for-the-web.rst:21
msgid ""
"Flatten all layers. This turns all your layers into a single one. Just go "
"to :menuselection:`Layer --> Flatten Image` or press the :kbd:`Ctrl + Shift "
"+ E` shortcut. Flattening can take a while, so if you have a big image, "
"don't be scared if Krita freezes for a few seconds. It'll become responsive "
"soon enough."
msgstr ""
"Platta ut alla lager. Det gör om alla lager till ett enda. Gå bara till :"
"`Lager --> Platta ut bild` eller använd genvägen :kbd:`Ctrl + Skift + E`. "
"Det kan ta en viss tid att platta ut, så om bilden är stor, bli inte skrämd "
"om Krita fryser några sekunder.  Det svarar snart igen."

#: ../../tutorials/saving-for-the-web.rst:23
msgid ""
"Convert the color space to 8bit sRGB (if it isn't yet). This is important to "
"lower the filesize, and PNG for example can't take higher than 16bit. :"
"menuselection:`Image --> Convert Image Color Space` and set the options to "
"**RGB**, **8bit** and **sRGB-elle-v2-srgbtrc.icc** respectively. If you are "
"coming from a linear space, uncheck **little CMS** optimisations"
msgstr ""
"Konvertera färgrymden till 8-bitars sRGB (om den inte redan har den). Det är "
"viktigt för att minska filstorleken, och exempelvis PNG kan inte hantera mer "
"än 16 bitar: menuselection:`Bild --> Konvertera bildens färgrymd` och ställ "
"in respektive alternativ till **RGB**, **8 bitar** och **sRGB-elle-v2-"
"srgbtrc.icc**. Om bilden kommer från en linjär färgrymd, avmarkera **little "
"CMS** optimering."

#: ../../tutorials/saving-for-the-web.rst:25
msgid ""
"Resize! Go to :menuselection:`Image --> Scale Image To New Size` or use the :"
"kbd:`Ctrl + Alt + I` shortcut. This calls up the resize menu. A good rule of "
"thumb for resizing is that you try to get both sizes to be less than 1200 "
"pixels. (This being the size of HD formats). You can easily get there by "
"setting the **Resolution** under **Print Size** to **72** dots per inch. "
"Then press **OK** to have everything resized."
msgstr ""
"Ändra storlek! Gå till :menuselection:`Bild --> Skala bild till ny storlek` "
"eller använd genvägen :kbd:`Ctrl + Alt + I`. Det visar "
"storleksändringsmenyn. En bra tumregel för storleksändring är att försöka få "
"båda storlekar mindre än 1200 bildpunkter. (Det är storleken på HD-format). "
"Det är enkelt att komma dit genom att ställa in **Upplösning** under "
"**Utskriftsstorlek** till **72** punkter per tum. Klicka därefter på **Ok** "
"för att ändra storlek på allting."

#: ../../tutorials/saving-for-the-web.rst:27
msgid ""
"Save as a web-safe image format. There's three that are especially "
"recommended:"
msgstr ""
"Spara med ett webbsäkert bildformat. Det finns tre som särskilt "
"rekommenderas:"

#: ../../tutorials/saving-for-the-web.rst:30
msgid "JPG"
msgstr "JPG"

#: ../../tutorials/saving-for-the-web.rst:32
msgid "Use this for images with a lot of different colors, like paintings."
msgstr "Använd det för bilder med många olika färger, såsom målningar."

#: ../../tutorials/saving-for-the-web.rst:35
msgid "PNG"
msgstr "PNG"

#: ../../tutorials/saving-for-the-web.rst:37
msgid ""
"Use this for images with few colors or which are black and white, like "
"comics and pixel-art. Select :guilabel:`Save as indexed PNG, if possible` to "
"optimise even more."
msgstr ""
"Använd det för bilder med få färger eller svartvita bilder, som serier eller "
"bildpunktskonst. Välj :guilabel:`Spara som indexerad PNG-bild, om möjligt` "
"för att optimera ännu mer."

#: ../../tutorials/saving-for-the-web.rst:40
msgid "GIF"
msgstr "GIF"

#: ../../tutorials/saving-for-the-web.rst:42
msgid ""
"Only use this for animation (will be supported this year) or images with a "
"super low color count, because they will get indexed."
msgstr ""
"Använd det bara för animering (kommer att stödjas i år) eller bilder med "
"särskilt lågt antal färger, eftersom de indexeras."

#: ../../tutorials/saving-for-the-web.rst:45
msgid "Saving with Transparency"
msgstr "Spara med genomskinlighet"

#: ../../tutorials/saving-for-the-web.rst:48
msgid ".. image:: images/Save_with_transparency.png"
msgstr ".. image:: images/Save_with_transparency.png"

#: ../../tutorials/saving-for-the-web.rst:49
msgid ""
"Saving with transparency is only possible with gif and png. First, make sure "
"you see the transparency checkers (this can be done by simply hiding the "
"bottom layers, changing the projection color in :menuselection:`Image --> "
"Image Background Color and Transparency`, or by using :menuselection:"
"`Filters --> Colors --> Color to Alpha`). Then, save as PNG and tick **Store "
"alpha channel (transparency)**"
msgstr ""
"Att spara med genomskinlighet är bara möjligt med gif och png. Säkerställ "
"först att schackmönstret för genomskinlighet syns (det går att göra genom "
"att helt enkelt dölja de undre lagren, ändra projektionsfärg under :"
"menuselection:`Bild --> Bildens bakgrundsfärg och genomskinlighet`, eller "
"genom att använda :menuselection:`Filter --> Färger --> Färg till alfa`). "
"Spara därefter som PNG och kryssa i **Lagra alfakanal (genomskinlighet)**."

#: ../../tutorials/saving-for-the-web.rst:51
msgid "Save your image, upload, and show it off!"
msgstr "Spara bilden, ladda upp den och förevisa den!"
