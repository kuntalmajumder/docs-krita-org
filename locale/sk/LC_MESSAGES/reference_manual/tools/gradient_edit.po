# translation of docs_krita_org_reference_manual___tools___gradient_edit.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___gradient_edit\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 13:19+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:18
msgid ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: toolgradientedit"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:1
msgid "Krita's vector gradient editing tool reference."
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Gradient"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:16
msgid "Gradient Editing Tool"
msgstr "Nástroj úpravy prechodov"

#: ../../reference_manual/tools/gradient_edit.rst:18
msgid "|toolgradientedit|"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:22
msgid ""
"This tool has been removed in Krita 4.0, and its functionality has been "
"folded into the :ref:`shape_selection_tool`."
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:24
msgid ""
"This tool allows you to edit the gradient on canvas, but it only works for "
"vector layers. If you have a vector shape selected, and draw a line over the "
"canvas, you will be able to see the nodes, and the stops in the gradient. "
"Move around the nodes to move the gradient itself. Select the stops to "
"change their color in the tool options docker, or to move their position in "
"the on canvas gradient. You can select preset gradient in the tool docker to "
"change the active shape's gradient to use those stops."
msgstr ""
