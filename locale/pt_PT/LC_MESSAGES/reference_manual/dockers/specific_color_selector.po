msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:38+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: YCrCb en ICC image KritaSpecificColorSelectorDocker\n"
"X-POFile-SpellExtra: LAB CMYK images XYZ dockers\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/dockers/specific_color_selector.rst:1
msgid "Overview of the specific color selector docker."
msgstr "Introdução à área de selecção de cores específicas."

#: ../../reference_manual/dockers/specific_color_selector.rst:11
#: ../../reference_manual/dockers/specific_color_selector.rst:16
msgid "Specific Color Selector"
msgstr "Selector de Cores Específicas"

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color Space"
msgstr "Espaço de Cores"

#: ../../reference_manual/dockers/specific_color_selector.rst:19
msgid ".. image:: images/dockers/Krita_Specific_Color_Selector_Docker.png"
msgstr ".. image:: images/dockers/Krita_Specific_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/specific_color_selector.rst:20
msgid ""
"The specific color selector allows you to choose specific colors within a "
"color space."
msgstr ""
"O selector de cores específico permite-lhe escolher cores específicas dentro "
"de um dado espaço de cores."

#: ../../reference_manual/dockers/specific_color_selector.rst:23
msgid "Color Space Chooser"
msgstr "Selector do Espaço de Cores"

#: ../../reference_manual/dockers/specific_color_selector.rst:25
msgid ""
"Fairly straightforward. This color space chooser allows you to pick the "
"color space, the bit depth and the icc profile in which you are going to "
"pick your color. Use the checkbox 'show color space selector' to hide this "
"feature."
msgstr ""
"É relativamente intuitivo. Este selector do espaço de cores permite-lhe "
"escolher o espaço de cores, a profundidade em 'bits' e o perfil ICC no qual "
"irá escolher a sua cor. Use a opção 'mostrar o selector do espaço de cores' "
"para esconder esta funcionalidade."

#: ../../reference_manual/dockers/specific_color_selector.rst:29
msgid "Sliders"
msgstr "Barras"

#: ../../reference_manual/dockers/specific_color_selector.rst:31
msgid ""
"These change per color space. If you chose 16bit float or 32 bit float, "
"these will go from 0 to 1.0, with the decimals deciding the difference "
"between colors."
msgstr ""
"Estas efectuam as mudanças dentro do espaço de cores. Se escolheu números de "
"vírgula flutuante de 16 ou 32 bits, eles irão de 0,0 a 1,0, usando a parte "
"decimal para decidir a diferença entre as cores."

#: ../../reference_manual/dockers/specific_color_selector.rst:35
msgid "Hex Color Selector"
msgstr "Selector de Cores Hexadecimal"

#: ../../reference_manual/dockers/specific_color_selector.rst:37
msgid ""
"This is only available for the color spaces with a depth of 8 bit. This "
"allows you to input hex color codes, and receive the RGB, CMYK, LAB, XYZ or "
"YCrCb equivalent, and the other way around!"
msgstr ""
"Isto só está disponível para os espaços de cores com uma profundidade de 8 "
"'bits'. Isto permite-lhe introduzir códigos de cores em hexadecimal e obter "
"os valores equivalentes em RGB, CMYK, LAB, XYZ ou YCrCb e vice-versa!"
